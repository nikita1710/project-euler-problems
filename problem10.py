# Project Euler - Problem 10
from math import sqrt, ceil
def sum_primes(limit):
    l = [True] * limit
    l[0] = False
    l[1] = False
    for i in range (2, ceil(sqrt(limit))+1):
        if l[i]:
            j = 2*i
            while j < limit:
                l[j] = False
                j += i

    # print([i for i in range(len(l)) if l[i]])

    s = 0
    for i in range (len(l)):
        if l[i]:
            s += i

    return s

# print (sum_primes(100))
print (sum_primes(2000000))

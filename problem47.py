# Project Euler - Problem 47
# Distinct Prime Factors

# from sympy import sieve
import prime

num = 644
count = 0
answer = 0

while True:
    if (len(set(prime.factors(num)))) == 4 :
        if count == 0:
            answer = num
        count += 1
        if count == 4:
            print(answer)
            break
        else:
            answer = 0
            count = 0
        num += 1

"""

def prime_factors(n):
    factors = []
    for i in range (1,n+1):
        if n%i == 0:
            factors.append(i)
"""

# Project Euler - Problem 16
# Power Digit Sum
# Reference - https://alphacentauri32.wordpress.com/2012/07/03/project-euler-problem-16-solved-with-python/
sum = 0
result = 2 ** 1000
str_result = str (result)

for i in str_result:
    sum = sum + int (i)

print (sum)

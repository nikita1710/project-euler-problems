# Project Euler - Problem 24
# Lexicographic Permutations
# Reference and Credits - https://github.com/nayuki/Project-Euler-solutions/blob/master/python/p024.py

import sys
import itertools

arr = list (range(10))
temp = itertools.islice(itertools.permutations(arr), 999999, None)
print ("".join(str(x) for x in next(temp)))

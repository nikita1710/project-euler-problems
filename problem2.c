// Project Euler - Problem 2
// Even fibonacci numbers
#include <stdio.h>
int main(int argc, char *argv[]) {
  int prev_first = 1;
  int prev_second = 2;
  int sum = 2; //2 is the first even-valued fibonacci number 
  int bound = 4000000;
  int i;
  for (i = 1; ; i++){
    int newFib = prev_first + prev_second;
    prev_first = prev_second;
    prev_second = newFib;
    // Check if the fibonacci number is greater than the bound value
    if (newFib > bound){
      break;
    }
    // Sum of even-valued numbers
    if (newFib % 2 == 0){
      sum += newFib;
    }
  }
  printf("Sum is %d ", sum);
  printf("\n");
  return 0;
}

/*
  References
  http://www.w3resource.com/euler-project/euler-problem2.php
  http://stackoverflow.com/questions/24483199/more-efficient-solution-project-euler-2-even-fibonacci-numbers
*/

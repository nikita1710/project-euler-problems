# Project Euler - Problem 12
# Highly divisible triangular number

# Function to give a list of triangular numbers from 1 to n
def triangular_number ():
    # Python generator for triangular numbers
    t = 1
    i = 2
    while True:
        yield t
        t += i
        i += 1

# Function to get the divisors of a number
def divisors (n):
    list_divisors = []
    for i in range(1, n+1):
        if n%i == 0:
            list_divisors.append(i)
    return list_divisors

tri = triangular_number()
for i in range(100):
    print(next(tri)) 

def highest_divisors (n):
    i = 1
    for j in range(2, n):
        i += j
        if len(divisors(i)) >= 500:
            return i
            break

print (highest_divisors(1000000))

// Project Euler - Problem 7
// 10001st Prime
import java.util.ArrayList;
public class Problem7{
  public static void main(String[] args) {
    int i = 0;
    int counter = 1;
    while (i < 10001){
      counter++;
      if (isPrime(counter)){
        i++;
      }
    }
    System.out.println(counter);
  }

  public static boolean isPrime(int n){
    for(int i=2;i<n;i++) {
        if(n%i==0)
            return false;
    }
    return true;
  }
}

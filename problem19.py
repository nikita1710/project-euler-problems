# Project Euler - Problem 19
# Counting Sundays

months_to_days = {
    0 : 31,
    1 : 28,
    2 : 31,
    3 : 30,
    4 : 31,
    5 : 30,
    6 : 31,
    7 : 31,
    8 : 30,
    9 : 31,
    10 : 30,
    11 : 31
}

def sundays():
    sunday_count = 0
    name = 0
    for year in range(1901, 2001):
        for month in range(12):
            if month == 1 and is_leap_year(year):
                day_range = 29
            else:
                day_range = months_to_days[month]
            for day in range(1, day_range):
                if day == 1 and name == 6:
                    sunday_count += 1

                name = (name + 1) % 7
    return sunday_count

def is_leap_year(year):
    return year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)

print(sundays())

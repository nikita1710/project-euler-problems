# Project Euler - Problem 20
# Factorial Digit Sum
from math import factorial
'''
def factorial(n):
    fact = 1
    for i in range(1,n+1):
        fact *= i
    return fact
'''

print(sum([int(i) for i in str(factorial(100))]))
'''
def factorial_digit_sum(fact):
    sum_digits = 0
    n = factorial(fact)
    while n > 0:
        ones = n % 10
        n = int((n-ones)/10)
        sum_digits += ones
    return sum_digits

print(factorial_digit_sum(100))
'''

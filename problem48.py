# Project Euler - Problem 48
# Self Powers

from math import pow

def self_powers(n):
    total = 0
    for i in range (1, n+1):
        total += i**i
    x = str(total)
    y = len(x)-10
    s = x[y:]
    return s

print(self_powers(1000))

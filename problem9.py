def pythagorean_triplet(limit):
    for a in range (1, limit):
        for b in range (a, limit-a):
            c = limit - a - b

            if a*a + b*b == c*c:
                return (a,b,c)

(a, b, c) = pythagorean_triplet(1000)
print(a, b, c)
print(a * b * c)

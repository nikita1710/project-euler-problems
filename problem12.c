// Project Euler - Problem 12
// Highly Divisible Triangular Number
#include<stdio.h>
#include<math.h>
int main(int argc, char *argv[]){
  long n, count, triangular_number;
  n = 1;
  while(n != 0){
    triangular_number = (n*(n+1))/2;
    count = 2;
    int k;
    for (k = 2; k <= sqrt(triangular_number); k++){
      if(triangular_number % k == 0){
        count += 2;
      }
      if(count > 500){
        printf("%ld\n",triangular_number);
        return 0;
      }
    }
    n++;
  }
  return 0;
}

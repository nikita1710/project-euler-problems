// Project Euler - Problem 6
// Sum square difference
#include <stdio.h>
#include <math.h>
int main (int argc, char *argv[]){
  int n = 0;
  int diff = 0;
  int sum1 = 0; // Sum of squares of n natural numbers
  int sum2 = 0; // Square of sum of n natural numbers
  printf("Enter the limit : ");
  scanf("%d",&n);
  sum1 = (n * (n+1) * ((2*n)+1))/6;
  sum2 = pow(((n*(n+1))/2),2);
  diff = sum2 - sum1;
  printf("Difference is %d",diff);
  printf("\n");
  return 0;
}

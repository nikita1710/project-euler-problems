# Project Euler - Problem 25
# 1000-digit Fibonacci Number

import math

def fibonacci(n):
    a = 1
    b = 2
    new = 0
    total = 0

    i = 3
    while len(str(new)) <= 999:
        new = a + b
        a, b = b, new
        i += 1
    return i

print(fibonacci(1000))

# Project Euler - Problem 29
# Distinct Powers

from math import pow

def distinct_powers():
    combinations = []
    for i in range(2,101):
        for j in range(2,101):
            n = pow(i,j)
            combinations.append(n)
    return len(set(combinations))

print(distinct_powers())

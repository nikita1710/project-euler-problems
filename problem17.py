# Thanks to Andrew 
num_words = {
1 : "one",
2 : "two",
3 : "three",
4 : "four",
5 : "five",
6 : "six",
7 : "seven",
8 : "eight",
9 : "nine",
10 : "ten",
11 : "eleven",
12 : "twelve",
13 : "thirteen",
14 : "fourteen",
15 : "fifteen",
16 : "sixteen",
17 : "seventeen",
18 : "eighteen",
19 : "nineteen",
20 : "twenty",
30 : "thirty",
40 : "forty",
50 : "fifty",
60 : "sixty",
70 : "seventy",
80 : "eighty",
90 : "ninety",
1000 : "onethousand"
}

def num_to_word(n):
    if n in num_words:
        return num_words[n]
    elif n < 100:
        ones = n % 10
        tens = (n - ones)
        return num_words[tens] + num_words[ones]
    elif n < 1000:
        ones = n % 10
        tens = n % 100
        hundreds = n - tens
        tens -= ones

        output = num_words[hundreds/100] + "hundred"

        if (tens != 0 or ones != 0):
            output += "and" + num_to_word(tens+ones)
        return output

total = 0
for i in range (1, 1001):
    total += len(num_to_word(i))
print(total)

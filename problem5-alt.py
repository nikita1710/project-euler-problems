# Project Euler - Problem 5
# Smallest Multiple
def smallest_multiple (limit):
    l = []
    for i in range (2, limit+1):
        for j in l:
            if i % j == 0:
                i = int(i/j)

        if i != 1:
            l.append(i)

    print (l)
    product = 1
    for i in l:
        product *= i

    return product

print(smallest_multiple(20))

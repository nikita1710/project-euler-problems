# Project Euler - Problem 44
# Pentagon Numbers

import math

def main():
    P = set((n * ((3 * n) - 1) / 2) for n in range(2, 4000))
    for i in P:
        for j in P:
            if i+j in P and j-i in P:
                x = i - j
            else:
                continue
    print(abs(x))

if __name__ == '__main__':
    main()

// Project Euler - Problem 4
// Largest Palindrome Product
#include <stdio.h>
// Function to check if a number is a palindrome or not
int is_palindrome(unsigned int n){
  unsigned int original = n, reversed = 0;
  // 1 - True; 0 - False
  if (n < 10){
    return 1;
  }
  if (n % 10 == 0){
    return 0;
  }
  while (n >= 1) {
    reversed = (reversed * 10) + (n % 10);
    n /= 10;
  }
  if (reversed == original){
    return 1;
  }
  else{
    return 0;
  }
}
int main (int argc, char *argv[]){
  int i, j, result;
  int max = 0;
  int palindrome = 0;
  for (i = 100; i <= 999; i++){
    for (j = 100; j <= 999; j++){
      result = i * j;
      if (is_palindrome(result) == 1 && result > max){
        max = result;
      }
    }
  }
  printf("Largest palindrome is : %d", max);
  printf ("\n");
  return 0;
}

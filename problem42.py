# Project Euler - Problem 42
# Coded Triangle Numbers
from math import sqrt

# Got this method after solving the quadratic equation x = n(n+1)/2
def check_triangle(k):
    x = sqrt(1+8*k)
    return x.is_integer()

def triangle_word(word):
    f = open("words.txt","r")
    counter = 0 # Triangle word counter
    words = [i.strip('"') for i in f.readline().split(",")]
    for word in words:
        word_value = sum([ord(i) - ord('A') + 1 for i in word])
        if check_triangle(word_value):
            counter += 1
    print (counter)
    return counter

triangle_word("x")

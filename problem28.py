# Project Euler - Problem 28
# Number Spiral Diagonals

from math import pow
"""
def number_spiral_diagonals(n):
    # result = (16*pow(n,3) + 26*n)/3 + 10 * pow(n,2) + 1
    # result = 1
    #for i in range(2,n+1):
    #    result += 4*n*n - 6*n + 6

    # return (16*pow(n, 3) + 26*n)/3 + 10*pow(n, 2)
    if n < 1:
        return None
    elif n == 1:
        return 1
    elif n % 2 == 0:
        return None
    else:

print(number_spiral_diagonals(1001)+1)
"""

total = 1
num = 1

for index in range(2,1001,2):
    for reps in range(4):
        num += index
        total += num


print total

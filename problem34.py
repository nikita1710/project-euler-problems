# Project Euler - Problem 34
# Digit Factorials
from math import factorial

def digits(n):
    # Returns true if a number is a curious number
    return sum([factorial(int(i)) for i in list(str(n))]) == n

def digit_factorials():
    total = 0
    for i in range(1, 1000001):
        if digits(i):
            total += i
    return total-3 # Since we are not taking 1! and 2! into account

print(digit_factorials())

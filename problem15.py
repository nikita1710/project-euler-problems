# Project Euler - Problem 15
# Lattice Paths
from math import factorial

def main():
    n = 40 # Total number of moves
    r = 20 # Number of moves to the right
    print ((factorial(n))/(factorial(r)*factorial(n-r)))

if __name__ == '__main__':
    main()

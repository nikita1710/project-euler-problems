# Project Euler - Problem 39
# Integer Right Triangles

"""
a, b, c - Sides of the Triangle
Perimeter = a + b + c
a^2 + b^2 = c^2
"""

from math import pow
from collections import defaultdict

def calculate_solutions (limit):
    res = defaultdict(list)
    for a in range(1, limit+1):
        for b in range(a, limit+1):
            for c in range(b, limit+1):
                if pow(a,2) + pow(b,2) == pow(c,2):
                    res[a+b+c].append((a,b,c))
    return res

def main():
    solutions = calculate_solutions(500)
    most = max(len(s) for s in list(solutions.values()))
    for p, s in sorted(solutions.items()):
        if len(s) == most:
            print(p)

if __name__ == "__main__":
    main()

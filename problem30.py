# Project Euler - Problem 30
# Digit Fifth Powers

def sum_digits_powers(n):
    sum_digits = 0
    for i in str(n):
        sum_digits += int(i) ** 5
    return sum_digits

result = 0
for i in range(2, 354295):
    if i == sum_digits_powers(i):
        result += i

print (result)

# Project Euler - Problem 45
# Triangular, Pentagonal and Hexagonal Numbers

# The code prints all triangular numbers in the given range. The second printed number is the answer
def main ():
    T = set((n * (n + 1) / 2) for n in range(2, 180000))
    P = set((n * ((3 * n) - 1) / 2) for n in range(2, 180000))
    H = set((n * ((2 * n) - 1)) for n in range(2, 180000))
    for i in T:
        if i in P and i in H:
            print(i)

        else:
            continue

if __name__ == '__main__':
    main()

"""
p = 165
h = 143
h = 84*p + 97*h - 38

print "Next triangle number =", h*(2*h - 1)
"""

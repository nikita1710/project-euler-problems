# Project Euler - Problem 22
# Names Scores

def name_score():
    f = open("names.txt","r")
    names = sorted([i.strip('"') for i in f.readline().split(',')])
    total_score = 0
    position_counter = 1
    for name in names:
        word_value = sum([ord(i) - ord('A') + 1 for i in name])
        score = position_counter * word_value
        position_counter += 1
        total_score += score
    return total_score

print(name_score())

# Project Euler - Problem 3
# Largest Prime Factor
from math import sqrt, ceil
def largest_factor(n):
    if n <= 3:
        return n
    for i in range (2, ceil(sqrt(n))+1):
        if n % i == 0:
            return largest_factor(n / i)

    return n

print(largest_factor(310))
print(largest_factor(128))
print(largest_factor(600851475143))

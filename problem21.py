def amicablePartner(n):
    return sum([i for i in range(1, n) if n % i == 0])

def isAmicable(a):
    b = amicablePartner(a)
    if b!=a:
        return amicablePartner(b) == a
    return False

def sumAmicable(limit):
    s = 0
    for i in range(1, limit):
        if isAmicable(i):
            s += i
    return s

print(sumAmicable(10000))

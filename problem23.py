# Project Euler - Problem 23
# Non-abundant sums
# Reference - http://pythonfiddle.com/non-abundant-sums/

"""
abundantNums = [12]
for x in range(13, 28123):
    if sum(divisors(x)) > x:
        abundantNums.append(x)

NonAbSums = [x for x in range(28124)]

for i in range(len(abundantNums)):
    for j in range(i, len(abundantNums)):
        currentSum = abundantNums[i] + abundantNums[j]
        if currentSum > 28123:
            break
        else:
            if currentSum in NonAbSums:
                NonAbSums.remove(currentSum)

print sum(NonAbSums)
"""

from math import sqrt

# Function that returns a list of divisors for a number
def divisors(n):
    list_divisors = [1]
    for i in range(2,int(sqrt(n))+1):
        if n%i == 0:
            list_divisors.append(i)
            list_divisors.append(int(n/i))
    return list_divisors

def non_abundant_sums():
    s = 0
    abundant_list = []
    for i in range (1, 28124):
        if (sum(divisors(i)) > i):
            abundant_list.append(i)
    abundant_set = set(abundant_list)
    non_abundant_set = set(range(1, 28124)) - abundant_set
    s = sum(list(non_abundant_set))
    return s

print(non_abundant_sums())

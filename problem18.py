# Project Euler - Problem 18
# Maximum Path Sum I

f = open("triangle.txt", "r")
triangle = []
for line in f:
    triangle.append(list(map(int, line.split(' '))))

print(triangle)
def max_sum(x, y):
    if y == len(triangle)-1:
        return triangle[y][x]
    else:
        return triangle[y][x] + max(max_sum(x, y+1), max_sum(x+1, y+1))

print(max_sum(0,0))

// Project Euler - Problem 1
// Multiples of threes and fives

#include<stdio.h>
int main (int argc, char *argv[]){
  int sum = 0;
  int i;
  for (i = 1; i < 1000; i++){
    if (i%3 == 0|| i%5 == 0){
      sum += i;
    }
  }
  printf("Sum is %d", sum);
  printf("\n");
  return 0;
}
  
